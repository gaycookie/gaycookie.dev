---
layout: post
title: "Goodbye Curseforge, hello Modrinth!"
date: 2023-05-31 02:30:00 + 0200
categories: development
---

Dear friends and fellow modding enthusiasts,

I wanted to take a moment to share an important decision I've made regarding my future modding endeavors. After careful consideration and evaluating my goals as a developer, I have decided to stop uploading my projects to Curseforge and instead focus on the vibrant and open-source-friendly community over at Modrinth.

This decision was primarily driven by my desire to support the Free and Open-Source Software (FOSS) movement to a greater extent. While Curseforge has undoubtedly been a popular platform for sharing mods, I believe that by shifting my efforts to Modrinth, I can contribute more effectively to the FOSS community and its principles.

One aspect that influenced my choice was my growing dissatisfaction with Curseforge's stance on mod loaders. It became apparent to me that their strong preference for Forge and limited support for alternative mod loaders did not align with my vision of inclusivity and diversity within the modding ecosystem. Modrinth, on the other hand, has shown a more open-minded approach, welcoming various mod loaders and encouraging innovation in modding practices.

By embracing Modrinth, I hope to foster an environment that promotes collaboration, creativity, and choice. I firmly believe that a diverse range of mod loaders can enrich the modding experience, offering users more options and encouraging healthy competition among developers.

I am truly excited about this new chapter and the possibilities that lie ahead. I kindly request you to join me on this journey by visiting my Modrinth [profile](https://modrinth.com/user/GayCookie) and exploring the projects I'll be sharing there. Your support and feedback mean the world to me, and I am eager to continue creating exciting and engaging mods for the community to enjoy.

Thank you for your understanding, and I look forward to seeing you on Modrinth!

Best regards,
GayCookie 🌈