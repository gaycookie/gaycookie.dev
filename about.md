---
layout: page
title: About Me
permalink: /about/
---

1. Introduction
2. Programming Passion
3. Favorite Programming Languages
4. Web Development Projects
5. Hobbies and Interests
  - Relaxing with Favorite Shows and Games
  - Love for Disaster Movies
  - Enjoyment of Action-Romantic Anime Movies
  - Fascination with Self-Hosting Services
  - Interest in Ham Radio
6. Exploring the Outdoors and Coffee
7. Conclusion

## **Hi there, I'm GayCookie: A Passionate Self-Taught Programmer**

Are you ready to delve into the world of programming and discover the journey of a self-taught programmer? Let me introduce myself. I'm GayCookie, a web developer hailing from the Netherlands, driven by an insatiable curiosity for all things related to coding. In this article, I'll share my favorite programming languages, my love for web development projects, and some intriguing hobbies that I pursue alongside my programming adventures.

## **Programming Passion**

From the moment I started my programming journey, I fell in love with the art of creating something out of nothing. The ability to bring ideas to life through code fascinated me, and I couldn't wait to learn more. Programming became my passion, and it continues to drive me to explore new technologies and develop innovative solutions.

## **Favorite Programming Languages**

While there are numerous programming languages out there, three have found a special place in my heart: JavaScript, TypeScript, and Python. Each language offers its unique strengths and areas of application. JavaScript allows me to add interactivity to web pages, TypeScript ensures safer and more scalable code, and Python provides simplicity and versatility for a wide range of projects.

## **Web Development Projects**

Web development projects are my playground, where I can turn ideas into reality. Whether it's crafting responsive websites or building dynamic web applications, I thrive on the challenges they present. Through these projects, I continuously expand my knowledge and keep up with the latest trends in the ever-evolving world of web development.

## **Hobbies and Interests**

Beyond the realm of programming, I have a diverse set of hobbies and interests that keep me inspired and energized. Let's take a closer look at some of them.

### Relaxing with Favorite Shows and Games

After a long day of coding, I find solace in relaxing with my favorite shows and games. One show that never fails to tickle my funny bone is Rick and Morty. Its clever humor and intriguing sci-fi concepts always manage to captivate me. When it comes to games, I can lose myself in the blocky world of Minecraft, where my creativity knows no bounds.

### Love for Disaster Movies

Disaster movies have a special place in my heart. There's something captivating about watching larger-than-life catastrophes unfold on the screen. Twister, 2012, The Day After Tomorrow, San Andreas and War of the Worlds are among my favorites, as they blend thrilling action with emotional storytelling. These movies serve as a reminder of the power of visual effects and storytelling to transport us to different worlds.

### Enjoyment of Action-Romantic Anime Movies

In addition to disaster movies, I have a soft spot for action-romantic anime movies. One movie that stands out is Your Name. Its beautifully animated scenes, enthralling storyline, and emotional depth make it my all-time favorite anime movie. The fusion of action, romance, and heartfelt storytelling in anime movies never fails to captivate my imagination.

### Fascination with Self-Hosting Services

Self-hosting services have become a captivating hobby for me. Through Docker, I enjoy setting up my own web servers, databases, and various other services. It not only empowers me to have control over my online presence but also offers valuable insights into the inner

 workings of web development and technology as a whole.

### Interest in Ham Radio

Amateur radio, commonly known as ham radio, is a fascinating world that has captured my interest. I'm currently in the process of learning more about this hobby, which allows me to communicate with people from all corners of the globe. The ability to connect with fellow enthusiasts and share knowledge through radio waves is an exciting prospect.

## **Exploring the Outdoors and Coffee**

While technology and programming occupy a significant part of my life, I also cherish moments of tranquility and connection with nature. Going for long walks and exploring the outdoors is a way for me to rejuvenate and find inspiration. As I venture through picturesque landscapes, I often accompany my journey with the comforting warmth of a cup of coffee, creating a perfect blend of relaxation and adventure.

## **Conclusion**

In conclusion, my programming journey has been a thrilling adventure, allowing me to create and explore in the vast world of technology. With a passion for web development and a love for languages like JavaScript, TypeScript, and Python, I'm constantly expanding my knowledge and honing my skills. Alongside coding, I indulge in hobbies such as watching shows, playing games, and enjoying movies that offer a perfect balance of action, romance, and emotion. The world of self-hosting services and ham radio further fuels my curiosity and broadens my understanding of technology. As I continue to explore the outdoors and sip on a warm cup of coffee, I look forward to the endless possibilities that programming and life have to offer.